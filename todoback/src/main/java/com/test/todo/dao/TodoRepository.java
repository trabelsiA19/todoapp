package com.test.todo.dao;

import com.test.todo.model.Todo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories("com.test.todo.dao")
public interface TodoRepository extends JpaRepository<Todo,Long> {
}
