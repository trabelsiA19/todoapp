package com.test.todo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import com.test.todo.dao.TodoRepository;
import com.test.todo.model.Todo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value="/rest/api")
@CrossOrigin(origins = "*")
public class TodoController {
    @Autowired
    TodoRepository todoRepository;

    @GetMapping(value="/allTodo")
    public List<Todo> allTodo(){
        return todoRepository.findAll();
    }

    @PostMapping(value="/addTodo")
    public Todo addTodo(@Valid @RequestBody Todo t){
        return todoRepository.save(t);
    }

    @GetMapping(value="/Todo/{id}")
    public ResponseEntity<Todo> getTodo(@PathVariable Long id) throws Exception{
        Todo t = todoRepository.findById(id).orElseThrow(()->new Exception("todo item  n'existe pas"));
        return ResponseEntity.ok().body(t);
    }

    @PutMapping(value="/Todo/{id}")
    public ResponseEntity<Todo> updateTodo(@PathVariable Long id,@Valid @RequestBody Todo todDetails) throws Exception{
        Todo t = todoRepository.findById(id).orElseThrow(()->new Exception("todo item n'exite  pas"));
     
        t.setComplete(todDetails.isComplete());
        Todo updatetod = todoRepository.save(t);
        return ResponseEntity.ok(updatetod);
    }

    @DeleteMapping(value="/Todo/{id}")
    public Map<String,Boolean> deleteTodo(@PathVariable Long id) throws Exception {
        Todo tod = todoRepository.findById(id).orElseThrow(()->new Exception(" todo item  n'existe pas"));
        todoRepository.delete(tod);
        Map<String,Boolean> response = new HashMap<>();
        response.put("todo item  est supprimé!",Boolean.TRUE);
        return response;
    }
}
