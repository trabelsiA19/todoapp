interface Todo {
    id: string
    name: string
    category: string
    complete: boolean
}

type TodoProps = {
    todo: ITodo
}

type ApiDataType = {
    message: string
    status: string
    todos: ITodo[]
    todo?: ITodo
  }
  