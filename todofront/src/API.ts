import axios, { AxiosResponse } from 'axios'

const baseUrl: string = 'http://localhost:8084/rest/api'

export const getTodos = async (): Promise<AxiosResponse<ApiDataType>> => {
  try {
    const todos: AxiosResponse<ApiDataType> = await axios.get(
      baseUrl + '/allTodo'
    )
    return todos
  } catch (error) {
    throw new Error()
  }
}

export const addTodo = async (
  formData: Todo
): Promise<AxiosResponse<ApiDataType>> => {
  try {
    const todo: Omit<Todo, 'id'> = {
      name: formData.name,
      category: formData.category,
      complete: false,
    }
    const saveTodo: AxiosResponse<ApiDataType> = await axios.post(
      baseUrl + '/addTodo',
      todo
    )
    window.location.reload(); 
    return saveTodo
  } catch (error) {
    throw new Error()
  }
}

export const updateTodo = async (
  todo: Todo
): Promise<AxiosResponse<ApiDataType>> => {
  try {
    const todoUpdate: Pick<Todo, 'complete'> = {
      complete: true,
    }
    const updatedTodo: AxiosResponse<ApiDataType> = await axios.put(
      `${baseUrl}/Todo/${todo.id}`,
      todoUpdate
    )
    window.location.reload(); 
    return updatedTodo
  } catch (error) {
    throw new Error()
  }
}

export const deleteTodo = async (
  todo: Todo
): Promise<AxiosResponse<ApiDataType>> => {
  try {
    const deletedTodo: AxiosResponse<ApiDataType> = await axios.delete(
      `${baseUrl}/Todo/${todo.id}`
    )
    window.location.reload(); 
    return deletedTodo
  } catch (error) {
    throw new Error()
  }
}
